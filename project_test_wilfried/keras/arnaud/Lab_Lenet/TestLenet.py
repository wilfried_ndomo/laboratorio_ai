
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.datasets import mnist
from keras import backend as K
import os
import matplotlib.pyplot as plt

from keras.models import load_model
#from keras.callbacks import TensorBoard
#from keras.callbacks import ModelCheckpoint

save_dir = os.path.join(os.getcwd(), 'saved_models1')
model_name = 'keras_mnist_trained_model.h5'
weights_name="weights.hdf5"

img_rows, img_cols = 28, 28
num_classes = 10
'''Load data from dataset mnist '''
_, (x_test, y_test) = mnist.load_data()

#Reshape size of image for 28*28
		
if K.image_data_format() == 'channels_first':
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_test = x_test.astype('float32')
x_test /= 255
y_test = keras.utils.to_categorical(y_test, num_classes)


def create_model():
    ''' model with reference Lenet5'''
    model = Sequential()
    model.add(Conv2D(6, (5, 5), padding='same',input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(16, (5, 5), padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(120))
    model.add(Activation('relu'))

    model.add(Dense(84))
    model.add(Activation('relu'))

    model.add(Dense(num_classes))
    model.add(Activation('softmax'))
    return model

if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
model_path = os.path.join(save_dir, model_name)
weights_path = os.path.join(save_dir, weights_name)
def test_model():
    '''print the result'''
    # Score trained model.
    model=create_model()
    model = load_model(model_path)
    #model.load_weights(weights_path)
    #opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    #model.compile(loss='categorical_crossentropy',optimizer=opt,metrics=['accuracy'])
    scores = model.evaluate(x_test, y_test, verbose=1)
    print('Test loss:', scores[0])
    print('Test accuracy:', scores[1])

test_model()
