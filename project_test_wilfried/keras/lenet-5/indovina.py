from keras.models import load_model
import cv2
import numpy as np

model = load_model('saved_models/keras_cifar10_trained_model.h5')

model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

img = cv2.imread('img7.png')
img = img.astype('float32')
img = img.astype('float32')
img /= 255
img /= 255
print (img.shape[0])
p=model.summary()
print(p)
#img = cv2.resize(img,(28,28))
#img = np.reshape(img,[1,28,28,1])
img = img.reshape(img.shape[0], 28, 28, 1)

classes = model.predict_classes(img)	

print (classes)