import tensorflow as tf

with tf.Session() as sess:
	#print(sess.run(product, feed_dict=d))
	v1=tf.get_variable(name="v1", initializer=1)
	v2=tf.get_variable(name="v2", initializer=[[1,2,3],[4,5,6],[7,8,9]])
	v3=tf.get_variable(name="PERMUTATIddON", initializer=[[0,0,1],[0,1,0],[1,0,0]])
	
	permutation = tf.matmul(v2,v3)
	mat=v2.assign(permutation)
	tf.global_variables_initializer().run(session=sess)
	sess.run(mat)
	
	
	print(v1)
	print(v1.eval(session=sess))
	
	v11=v1.assign_add(4)
	sess.run(v11)
	
	print(v1)
	print(v1.eval(session=sess))
	






