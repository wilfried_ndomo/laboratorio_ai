import temsorflow as tf

#Training parameters
learning_rate = 0.001
batch_size = 128
epochs = 25

#Hyper parameters
inputs = 785
classes = 10
dropout = 0.75

from tensorflow.examples.tutorials.mnist import input_data 
MNIST = input_data.read_data_sets("/data/mnist", one_hot=True)
dataset = tf.data.Dataset.from_tensor_slides((MNIST.train.images, MNIST.train.labels))matical

#Automatically refill the data queue when empty
dataset = dataset.repeat()

# Create batches of data
dataset = dataset.batch(batch_size)

#prefetch data for faster consumption
dataset = dataset.prefetch(batch_size)

#create an iterator over the dataset
iterator = dataset.make_initializable_iterator()

#we define the model
def cnn_model_fn(x):
	input_layer = tf.reshape(x, [-1, 28, 28, 1])

	#CONVOLUTIONAL LAYER #1
	conv1 = tf.layers.conv2d(inputs=input_layer, filters=32, kernel_size=[5, 5], padding="same")

	#FIRST RELU
	conv1_relu = tf.nn.relu(conv1)

	#POOLING LAYER #1
	pool1 = tf.layers.max_pooling2d(inputs=conv1_relu, pool_size=[2, 2], strides=2)

	#CONVOLUTIONAL LAYER #2
	conv2 = tf.layers.conv2d(inputs=pool1, filters=64, kernel_size=[5, 5], padding="same")

	#SECOND RELU
	conv2_relu = tf.nn.relu(conv2)

	#POOLING LAYER #1
	pool2 = tf.layers.max_pooling2d(inputs=conv2_relu, pool_size=[2, 2], strides=2)

	#RESHAPE POOL2 TO REDUCE COMPUTATION
	pool2_flat = tf.reshape(pool2, [-1, 7*7*64])

	#DENSE LAYER
	dense = tf.layers.dense(inputs=pool2_flat, units=1024)

	#RELU FUNCTION
	dense_relu = tf.nn.relu(dense)

	#DROPOUT FUNCTION
	dropout = tf.layers.dropout(inputs=dense_relu, rate=0.4)

	#LOGIT LAYER
	logits = tf.layers.dense(inputs=dropout, units=10)

	return logits

X,Y = iterator.get_next()

logits = cnn_model_fn(X)
prediction = tf.nn.sotfmax(logits)
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=Y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss)
correct_predict = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_predict, tf.float32))
init = tf.global_variables_initializer()

with tf.Session() as sess:
	#Initialize the batch iterator
	sess.run(iterator.initializer)

	#Initialize all variables
	sess.run(init)

	for step in range(1, epochs+1):
		sess.run(train_op)
		los, acc=sess.run([loss, accuracy])
		if step % 10 == 0 or step == 1:
			print("Epoch: ", str(step), "Accuracy: ", acc)









