import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
import os
import matplotlib.pyplot as plt
from keras.callbacks import EarlyStopping, ModelCheckpoint


def create_model(num_classes, x_train):
    model = Sequential()

    #Conv1
    model.add(Conv2D(64, (5, 5), padding='same',  
                                 input_shape=x_train.shape[1:]))
    model.add(Activation('relu'))
    model.add(Dropout(0.25))
    #Conv2
    model.add(Conv2D(64, (5, 5)))
    model.add(Activation('relu'))

    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    #Conv3
    model.add(Conv2D(96, (4, 4), padding='same'))
    model.add(Activation('relu'))
    model.add(Dropout(0.25))
    #Conv4
    model.add(Conv2D(96, (4, 4)))
    model.add(Activation('relu'))

    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    #Conv5
    model.add(Conv2D(128, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Dropout(0.25))
    #Conv6
    model.add(Conv2D(128, (3, 3), padding='same'))
    model.add(Activation('relu'))

    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())

    model.add(Dense(1024))
    model.add(Activation('relu'))

    model.add(Dense(512))
    model.add(Activation('relu'))

    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    return model

def train(batch_size, epochs, lr,
        x_train, y_train, x_test, y_test, 
        save_dir, weights_name, num_classes):

    '''training  the model using RMSprop '''
    model=create_model(num_classes, x_train)

    opt = keras.optimizers.rmsprop(lr=lr, decay=1e-6)
    
    model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255

    weights_path = os.path.join(save_dir, weights_name)

    earlystopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=50,
                                  verbose=1, mode='auto')

    checkpointer = ModelCheckpoint(filepath=weights_path, 
                                   verbose=1, save_best_only=True)

    history=model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True,
              callbacks=[earlystopping, checkpointer])
    return history, model