import keras
from keras.datasets import cifar10
from keras import backend as K
import os
import matplotlib.pyplot as plt

import deep

'''Hyper parameters'''
lr = 0.001
batch_size = 128
epochs = 50
num_classes = 10

'''command to save a models after training'''
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_cifar10_trained_model.h5'
weights_name="weights.hdf5"
if not os.path.isdir(save_dir):
        os.makedirs(save_dir)

# The data, split between train and test sets:
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


history, model=deep.train(batch_size, epochs, lr,
                x_train, y_train, x_test, y_test, 
                save_dir, weights_name, num_classes)


FILE = "logs.txt"
    
f=open(FILE, "w")
f.write("\n")
f=open(FILE, "w")
f.write("\n")
f.write(time.strftime("%H:%M:%S") + " " + time.strftime("%d/%m/%Y") + "\n\n")
f.write("Model accuracy " + str(history.history['acc'])+ "\n\n")
f.write("Validation accuracy " + str(history.history['val_acc'])+ "\n\n")
f.write("Model loss " + str(history.history['loss'])+ "\n\n")
f.write("Validation loss " + str(history.history['val_loss'])+ "\n\n")
f.write("training time: " + str(training_time))
f.write('\n')
f.close()


# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig("accuracy.png")
plt.close()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')

plt.savefig("loss.png")
plt.close()