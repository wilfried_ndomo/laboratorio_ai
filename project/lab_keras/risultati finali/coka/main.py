from __future__ import print_function
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras import backend as K
import os
import matplotlib.pyplot as plt
#import python-matplotlib.pyplot as plt
from keras.callbacks import ModelCheckpoint
import deep
import time

'''Hyper parameters'''
lr = 0.001
batch_size = 128
epochs = 50
num_classes = 10

'''command to save a models after training'''
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_cifar10_trained_model.h5'
weights_name="weights.hdf5"
if not os.path.isdir(save_dir):
        os.makedirs(save_dir)

# The data, split between train and test sets:
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# Convert class vectors to binary class matrices.
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

start_time = time.time()

history, model=deep.train(batch_size, epochs, lr,
                x_train, y_train, x_test, y_test, 
                save_dir, weights_name, num_classes)

training_time = time.time() - start_time

FILE = "logs.txt"
    
f=open(FILE, "w")
#for i in range (0, len(model.layers)):
	#f.write("Layer " + str(i) + ": " + str(model.layers[i].get_config()) + "\n")
f.write("\n")
f.write("Batch size: " + str(batch_size) + ", lr: " + str(lr) + ", loss: binary_crossentropy , metrics: accuracy, epochs: "+ str(epochs) +"\n\n")
#f.write("Batch size: " + str(batch_size) + ", lr: " + str(lr) + ", loss: " + loss + ", metrics: " + metrics + ", epochs: "+ str(epochs) +"\n\n")
f.write(time.strftime("%H:%M:%S") + " " + time.strftime("%d/%m/%Y") + "\n\n")
f.write("Model accuracy " + str(history.history['acc'])+ "\n\n")
f.write("Validation accuracy " + str(history.history['val_acc'])+ "\n\n")
f.write("Model loss " + str(history.history['loss'])+ "\n\n")
f.write("Validation loss " + str(history.history['val_loss'])+ "\n\n")
f.write("training time: " + str(training_time))
f.write('\n')
f.close()


# list all data in history
print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')


plt.savefig("GPUaccuracy.png")
plt.close()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')

plt.savefig("GPUloss.png")
plt.close()




