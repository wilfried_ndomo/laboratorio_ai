import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from data import get_data_set
#from tensorflow.examples.tutorials.mnist import input_data
import sys
import os

os.environ["CUDA_VISIBLE_DEVICES"]="0"
##Hyperparameter
training_iters = 50
learning_rate = 0.001 
batch_size = 128

n_input = 32
n_classes = 10
dropout = 0.25
##############################

train_x, train_y = get_data_set("train")
test_x, test_y = get_data_set("test")
label_dict = {
 0: 'plane',
 1: 'car',
 2: 'bird',
 3: 'cat',
 4: 'deer',
 5: 'dog',
 6: 'frog',
 7: 'horse',
 8: 'ship',
 9: 'truck',
}


train_x = train_x.reshape(-1,32,32,3)
test_x = test_x.reshape(-1,32,32,3)

x = tf.compat.v1.placeholder(tf.float32, [None, 32,32,3])
y = tf.compat.v1.placeholder(tf.float32, [None, n_classes])
keep_prob = tf.compat.v1.placeholder(tf.float32) # dropout (keep probability)

# Create some wrappers for simplicity
def conv2d(x, W, b, strides=1):
	x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='SAME')
	x = tf.nn.bias_add(x, b)
	return x

def maxpool2d(x, k=2):
	return tf.nn.max_pool2d(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],padding='SAME')

# Create model
def conv_net(x, weights, biases):
	conv1 = conv2d(x, weights['wc1'], biases['bc1'])
	conv1 = tf.nn.relu(conv1)
	conv1 = tf.nn.dropout(conv1,0.25)
	conv2 = conv2d(conv1, weights['wc2'], biases['bc2'])
	conv2 = tf.nn.relu(conv2)
	conv2 = maxpool2d(conv2, k=2)
	conv2 = tf.nn.dropout(conv2,0.25)
	
	conv3 = conv2d(conv2, weights['wc3'], biases['bc3'])
	conv3 = tf.nn.relu(conv3)
	conv3 = tf.nn.dropout(conv3,0.25)
	conv4 = conv2d(conv3, weights['wc4'], biases['bc4'])
	conv4 = maxpool2d(conv4, k=2)
	conv5 = conv2d(conv4, weights['wc5'], biases['bc5'])
	conv5 = tf.nn.dropout(conv5,0.25)
	conv6 = conv2d(conv5, weights['wc6'], biases['bc6'])
	conv6 = maxpool2d(conv6, k=2)

	# Fully connected layer
	fc1 = tf.reshape(conv6, [-1, weights['wd1'].get_shape().as_list()[0]])
	fc1 = tf.add(tf.matmul(fc1, weights['wd1']), biases['bd1'])
	fc1 = tf.nn.relu(fc1)
	fc2 = tf.add(tf.matmul(fc1, weights['wd2']), biases['bd2'])
	fc2 = tf.nn.relu(fc2)
	fc3 = tf.add(tf.matmul(fc2, weights['wd3']), biases['bd3'])
	fc3 = tf.nn.relu(fc3)
	out = tf.add(tf.matmul(fc3, weights['out']), biases['out'])
	return out


weights = {
	'wc1': tf.Variable(tf.random.normal([5,5,3,64])), 
    'wc2': tf.Variable(tf.random.normal([5,5,64,64])), 
    'wc3': tf.Variable(tf.random.normal([4,4,64,96])), 
    'wc4': tf.Variable(tf.random.normal([4,4,96,96])),
    'wc5': tf.Variable(tf.random.normal([3,3,96,128])),
    'wc6': tf.Variable(tf.random.normal([3,3,128,128])),
    'wd1': tf.Variable(tf.random.normal([4*4*128,128])),
    'wd2': tf.Variable(tf.random.normal([128,1024])),
    'wd3': tf.Variable(tf.random.normal([1024,512])),
    'out': tf.Variable(tf.random.normal([512,n_classes])), 

}
biases = {

	'bc1': tf.Variable(tf.random.normal([64])),
    'bc2': tf.Variable(tf.random.normal([64])),
    'bc3': tf.Variable(tf.random.normal([96])),
    'bc4': tf.Variable(tf.random.normal([96])),
    'bc5': tf.Variable(tf.random.normal([128])),
    'bc6': tf.Variable(tf.random.normal([128])),
    'bd1': tf.Variable(tf.random.normal([128])),
    'bd2': tf.Variable(tf.random.normal([1024])),
    'bd3': tf.Variable(tf.random.normal([512])),
    'out': tf.Variable(tf.random.normal([10])),
}

logits = conv_net(x, weights, biases)
pred = tf.nn.softmax(logits)
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=y))
op=tf.compat.v1.train.AdamOptimizer(learning_rate=learning_rate)
optimizer = op.minimize(cost)
correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
init = tf.compat.v1.global_variables_initializer()

with tf.compat.v1.Session()  as sess:
    sess.run(init) 
    train_loss = []
    test_loss = []
    train_accuracy = []
    test_accuracy = []
    summary_writer = tf.compat.v1.summary.FileWriter('./Output', sess.graph)
    for i in range(training_iters):
        for batch in range(len(train_x)//batch_size):
            batch_x = train_x[batch*batch_size:min((batch+1)*batch_size,len(train_x))]
            batch_y = train_y[batch*batch_size:min((batch+1)*batch_size,len(train_y))]    
            # Run optimization op (backprop).
            # Calculate batch loss and accuracy
            opt = sess.run(optimizer, feed_dict={x: batch_x,y: batch_y})
            loss, acc = sess.run([cost, accuracy], feed_dict={x: batch_x,y: batch_y})
        print("Iter " + str(i) + ", Loss= " + \
                      "{:.6f}".format(loss) + ", Training Accuracy= " + \
                      "{:.5f}".format(acc))
        print("Optimization Finished!")

        # Calculate accuracy for all 10000 mnist test images
        test_acc,valid_loss = sess.run([accuracy,cost], feed_dict={x: test_x,y : test_y})
        train_loss.append(loss)
        test_loss.append(valid_loss)
        train_accuracy.append(acc)
        test_accuracy.append(test_acc)
        print("Testing Accuracy:","{:.5f}".format(test_acc))
        print("Testing Loss:","{:.5f}".format(valid_loss))
    summary_writer.close()


plt.plot(range(len(train_accuracy)), train_accuracy, 'b', label='Accuracy Train')
plt.plot(range(len(test_accuracy)), test_accuracy, 'r', label='Accuracy Test')
plt.title('Accuracy Training and Test')
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Accuracy',fontsize=16)
plt.legend(['train', 'test'], loc='upper left')
plt.savefig("accuracy.png")
plt.close()




plt.plot(range(len(train_loss)), train_loss, 'b', label='Training loss')
plt.plot(range(len(test_loss)), test_loss, 'r', label='Test loss')
plt.title('Training and Test loss')
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Loss',fontsize=16)
plt.legend(['train', 'test'], loc='upper left')
plt.savefig("loss.png")
plt.close()

