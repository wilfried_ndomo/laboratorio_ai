import sys
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler
import torch
import numpy as np
import torch.optim as optim

# check if CUDA is available
train_on_gpu = torch.cuda.is_available()

if not train_on_gpu:
    print('CUDA is not available.  Training on CPU ...')
else:
    print('CUDA is available!  Training on GPU ...')

# number of subprocesses to use for data loading
num_workers = 0
# how many samples per batch to load
batch_size = 128
# percentage of training set to use as validation
valid_size = 0.2

# convert data to a normalized torch.FloatTensor
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

# choose the training and test datasets
train_data = datasets.CIFAR10('data', train=True,
                              download=True, transform=transform)
test_data = datasets.CIFAR10('data', train=False,
                             download=True, transform=transform)

# obtain training indices that will be used for validation
num_train = len(train_data)
indices = list(range(num_train))
np.random.shuffle(indices)
split = int(np.floor(valid_size * num_train))
train_idx, valid_idx = indices[split:], indices[:split]

# define samplers for obtaining training and validation batches
train_sampler = SubsetRandomSampler(train_idx)
valid_sampler = SubsetRandomSampler(valid_idx)

# prepare data loaders (combine dataset and sampler)
train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size,
    sampler=train_sampler, num_workers=num_workers)
valid_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, 
    sampler=valid_sampler, num_workers=num_workers)
test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, 
    num_workers=num_workers)

# specify the image classes
classes = ['airplane', 'automobile', 'bird', 'cat', 'deer','dog', 'frog', 'horse', 'ship', 'truck']
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 64, 5, padding=1)
        self.conv2 = nn.Conv2d(64, 64, 5, padding=1)
        self.conv3 = nn.Conv2d(64, 96, 4, padding=1)
        self.conv4 = nn.Conv2d(96, 96, 4, padding=1)
        self.conv5 = nn.Conv2d(96, 128, 3, padding=1)
        self.conv6 = nn.Conv2d(128, 128, 3, padding=1)
        # max pooling layer
        self.pool = nn.MaxPool2d(2, 2)
        # fully connected layers
        self.fc1 = nn.Linear(128 * 3 * 3, 1024)
        self.fc2 = nn.Linear(1024, 512)
        self.fc3 = nn.Linear(512, 10)
        self.dropout = nn.Dropout(p=.25)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.dropout(x)
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        x = self.dropout(x)

        x = F.relu(self.conv3(x))
        x = self.dropout(x)
        x = F.relu(self.conv4(x))
        x = self.pool(x)
        x = self.dropout(x)

        x = F.relu(self.conv5(x))
        x = self.dropout(x)
        x = F.relu(self.conv6(x))
        x = self.pool(x)
        x = self.dropout(x)

        x = x.view(-1, 3*3*128)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        return x


model = Net()

# move tensors to GPU if CUDA is available
if train_on_gpu:
    model.cuda()

# specify loss function
criterion = nn.CrossEntropyLoss()

# specify optimizer
optimizer = optim.SGD(model.parameters(), lr=.001)

n_epochs = 50 # you may increase this number to train a final model
running_loss_history = []
running_correct_history = []
validation_running_loss_history = [] 
validation_running_correct_history = []

valid_loss_min = np.Inf # track change in validation loss

for epoch in range(1, n_epochs+1):

    # keep track of training and validation loss
    train_loss = 0.0
    valid_loss = 0.0
    running_loss = 0.0
  
    running_correct = 0.0
    validation_running_loss = 0.0
    validation_running_correct = 0.0
    
    
    #######Trian model ############
    model.train()
    for data, target in train_loader:
        
        if train_on_gpu:
            data, target = data.cuda(), target.cuda() # clear the gradients of all optimized variables
        optimizer.zero_grad()  # forward pass: compute predicted outputs by passing inputs to the model
        output = model(data)   # calculate the batch loss
        loss = criterion(output, target) # backward pass: compute gradient of the loss with respect to model parameters
        loss.backward()   # perform a single optimization step (parameter update)
        optimizer.step()  # update training loss
        train_loss += loss.item()*data.size(0)
        _, preds = torch.max(output, 1)
        running_correct += torch.sum(preds == target.data)
        running_loss += loss.item()



    
    ####Validate model##################
    model.eval()
    for data, target in valid_loader:
        # move tensors to GPU if CUDA is available
        if train_on_gpu:
            data, target = data.cuda(), target.cuda()
            # forward pass: compute predicted outputs by passing inputs to the model
        output = model(data)
        # calculate the batch loss
        loss = criterion(output, target)
        valid_loss += loss.item()*data.size(0)
        _, val_preds = torch.max(output, 1)
        validation_running_loss += loss.item()
        validation_running_correct += torch.sum(val_preds == target.data) 

    epoch_loss = running_loss / len(train_loader)
    epoch_acc = running_correct.float() / len(train_loader)
    running_loss_history.append(epoch_loss)
    running_correct_history.append(epoch_acc.item())
    
    val_epoch_loss = validation_running_loss / len(valid_loader)
    val_epoch_acc = validation_running_correct.float() / len(valid_loader)
    validation_running_loss_history.append(val_epoch_loss)
    validation_running_correct_history.append(val_epoch_acc)

    
    print("===================================================")
    print("epoch: ", epoch)
    print("training loss: {:.5f}, acc: {:5f}".format(epoch_loss, epoch_acc))
    print("validation loss: {:.5f}, acc: {:5f}".format(val_epoch_loss, val_epoch_acc))
    print(running_loss_history)
    print(running_correct_history)




FILE = "logs.txt"
f=open(FILE, "w")

f.write("\n")
f.write("Batch size: " + str(batch_size) + ", lr: 0.001" + ", loss: binary_crossentropy , metrics: accuracy, epochs: "+ str(epoch) +"\n\n")

f.write("Model accuracy " + str(epoch_acc) + "\n\n")
f.write("Validation accuracy " + str(val_epoch_acc)+ "\n\n")
f.write("Model loss " + str(epoch_loss) + "\n\n")
f.write("Validation loss " + str(val_epoch_loss) + "\n\n")

f.write('\n')
f.close()


plt.plot(range(len(running_correct_history)), running_correct_history, 'b', label='Accuracy Train')
plt.plot(range(len(validation_running_correct_history)), validation_running_correct_history, 'r', label='Accuracy Test')
plt.title('Accuracy Training and Test')
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Accuracy',fontsize=16)
plt.legend(['train', 'test'], loc='upper left')
plt.savefig("accuracy.png")
plt.close()




plt.plot(range(len(running_loss_history)), running_loss_history, 'b', label='Training loss')
plt.plot(range(len(validation_running_loss_history)), validation_running_loss_history, 'r', label='Test loss')
plt.title('Training and Test loss')
plt.xlabel('Epochs ',fontsize=16)
plt.ylabel('Loss',fontsize=16)
plt.legend(['train', 'test'], loc='upper left')
plt.savefig("loss.png")
plt.close()



        
     



