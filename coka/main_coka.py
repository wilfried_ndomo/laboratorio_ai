import tensorflow as tf
import tensorflow.keras
from tensorflow.keras import backend as K 
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import Adam
from sklearn.metrics import confusion_matrix
import itertools
import os, sys, random
import time
import math
from cnn_model import create_model
#from plot_confusion_matrix import plot_confusion_matrix

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

start_time = time.time()

NAME = "8-layers"

# ALCUNI IPERPARAMETRI
lr = 0.001
loss= 'binary_crossentropy'
metrics = 'accuracy'
epochs = 8
dataset='501010'
batch_size=500

log = "/home/nfazzini/coka_exp_01/logs/" + dataset + "{}.txt".format(int(time.time()))


TRAINDIR = '/home/nfazzini/datasets/' + dataset + '/TRAIN_IMG'
VALIDDIR = '/home/nfazzini/datasets/' + dataset + '/VALID_IMG'
TESTDIR = '/home/nfazzini/datasets/' + dataset + '/TEST_IMG'


train_batches = ImageDataGenerator().flow_from_directory(
    TRAINDIR, 
    target_size=(1000,48),
    color_mode='grayscale', 
    classes=['SELECTION', 'NEUTRAL'], 
    batch_size=int(batch_size))
valid_batches = ImageDataGenerator().flow_from_directory(
    VALIDDIR, 
    target_size=(1000,48),
    color_mode='grayscale', 
    classes=['SELECTION', 'NEUTRAL'], 
    batch_size=int(batch_size))
test_batches = ImageDataGenerator().flow_from_directory(
    TESTDIR, 
    target_size=(1000,48),
    color_mode='grayscale', 
    classes=['SELECTION', 'NEUTRAL'], 
    batch_size=int(batch_size))
    
   
K.clear_session()
model = create_model(DEBUG=True)
#model.summary()

model.compile(Adam(lr=lr), loss=loss, metrics=[metrics])


namefile = sys.argv[0]
namelog = namefile + dataset
#csv_logger = CSVLogger('/home/nfazzini/coka_exp_01/logs/' + namelog+'_{}'.format(int(time.time())))

epoch_steps = len(train_batches)
valid_steps = len(valid_batches)

# CALLBACKS ERRORS: https://github.com/keras-team/keras/issues/3358
# tensorboard --logdir=tb/

#tbCallback = TensorBoard(
#log_dir='tb/{}'.format(NAME), 
#histogram_freq=0, #cambia  
#write_graph=True, 
#write_images=True)

# # codice inefficiente, carica tutto la cartella VALID_IMAGE in memoria prima del training
# class TensorBoardWrapper(TensorBoard):
#     '''Sets the self.validation_data property for use with TensorBoard callback.'''

#     def __init__(self, batch_gen, nb_steps, **kwargs):
#         super().__init__(**kwargs)
#         self.batch_gen = batch_gen # The generator.
#         self.nb_steps = nb_steps     # Number of times to call next() on the generator.

#     def on_epoch_end(self, epoch, logs):
#         # Fill in the `validation_data` property. Obviously this is specific to how your generator works.
#         # Below is an example that yields images and classification tags.
#         # After it's filled in, the regular on_epoch_end method has access to the validation_data.
#         imgs, tags = None, None
#         for s in range(self.nb_steps):
#             ib, tb = next(self.batch_gen)
#             if imgs is None and tags is None:
#                 imgs = np.zeros((self.nb_steps * ib.shape[0], *ib.shape[1:]), dtype=np.float32)
#                 tags = np.zeros((self.nb_steps * tb.shape[0], *tb.shape[1:]), dtype=np.uint8)
#             imgs[s * ib.shape[0]:(s + 1) * ib.shape[0]] = ib
#             tags[s * tb.shape[0]:(s + 1) * tb.shape[0]] = tb
#         self.validation_data = [imgs, tags, np.ones(imgs.shape[0]), 0.0]
#         return super().on_epoch_end(epoch, logs)

# fixed code? --> da assertionerror in return
    # class TensorBoardWrapper(TensorBoard):
    #     '''Sets the self.validation_data property for use with TensorBoard callback.'''

    #     def __init__(self, batch_gen, nb_steps, b_size, **kwargs):
    #         super(TensorBoardWrapper, self).__init__(**kwargs)
    #         self.batch_gen = batch_gen # The generator.
    #         self.nb_steps = nb_steps   # Number of times to call next() on the generator.
    #         #self.batch_size = b_size

    #     def on_epoch_end(self, epoch, logs):
    #         # Fill in the `validation_data` property. Obviously this is specific to how your generator works.
    #         # Below is an example that yields images and classification tags.
    #         # After it's filled in, the regular on_epoch_end method has access to the validation_data.
    #         imgs, tags = None, None
    #         for s in range(self.nb_steps):
    #             ib, tb = next(self.batch_gen)
    #             if imgs is None and tags is None:
    #                 imgs = np.zeros(((self.nb_steps * self.batch_size,) + ib.shape[1:]), dtype=np.float32)
    #                 tags = np.zeros(((self.nb_steps * self.batch_size,) + tb.shape[1:]), dtype=np.uint8)
    #             imgs[s * ib.shape[0]:(s + 1) * ib.shape[0]] = ib
    #             tags[s * tb.shape[0]:(s + 1) * tb.shape[0]] = tb
            
    #         self.validation_data = [imgs, tags, np.ones(imgs.shape[0])]
                
    #         return super(TensorBoardWrapper, self).on_epoch_end(epoch, logs)

# il dataset deve essere bilanciato
# tbCallback1 = TensorBoardWrapper(
#     valid_batches, 
#     nb_steps=5, 
#     log_dir='tb/{}'.format(NAME), 
#     histogram_freq=1,
#     batch_size=int(batch_size), 
#     write_graph=False, 
#     write_grads=True,
#     write_images=True
# )

history = model.fit_generator(
    train_batches, 
    steps_per_epoch=epoch_steps, 
    validation_data=valid_batches,
    validation_steps=valid_steps, 
    epochs=epochs,
    verbose=0,
    #callbacks=[TQDMCallback()]
    #callbacks=[tbCallback1, csv_logger]
    #callbacks=[tbCallback, csv_logger]
    #callbacks=callback
    #callbacks=[csv_logger]
    )

elapsed_time = time.time() - start_time
    
FILE = dataset+"_{}_{}_{}_{}.h5".format(str(batch_size),str(lr),str(epochs),int(time.time()))
model.save('/home/nfazzini/coka_exp_01/models/' + FILE)

evaluations = model.evaluate_generator(
    test_batches, 
    steps=len(test_batches),
    workers=1, 
    use_multiprocessing=False, 
    verbose=0)

test_batches.reset() #Necessary to force it to start from beginning
predictions = model.predict_generator(
    test_batches, 
    steps=len(test_batches), 
    verbose=0)
# seleziona la classe che ha la maggior probabilità -> [0,1] selection, [1,0] neutral

test_files_n = os.listdir(TESTDIR+'/NEUTRAL')
test_files_s = os.listdir(TESTDIR+'/SELECTION')
test_set_size = len(test_files_n) + len(test_files_s)

classes = test_batches.classes[test_batches.index_array]

rounded_pred=np.argmax(predictions,axis=-1)
y_pred=[]
for i in range(0, len(rounded_pred)):
    y_pred.append(int(rounded_pred[i]))
y_pred=np.array(y_pred)
        
FILE1 = 'logs/'+dataset+"_{}_{}_{}_{}.txt".format(str(batch_size),str(lr),str(epochs),int(time.time()))
    
f=open(FILE1, "w")
for i in range (0, len(model.layers)):
	f.write("Layer " + str(i) + ": " + str(model.layers[i].get_config()) + "\n")
f.write("\n")
f.write("Batch size: " + str(batch_size) + ", lr: " + str(lr) + ", loss: " + loss + ", metrics: " + metrics + ", epochs: "+ str(epochs) +"\n\n")
f.write(time.strftime("%H:%M:%S") + " " + time.strftime("%d/%m/%Y") + "\n\n")
f.write("Model accuracy " + str(history.history['acc'])+ "\n")
f.write("Validation accuracy " + str(history.history['val_acc'])+ "\n")
f.write("Model loss " + str(history.history['loss'])+ "\n")
f.write("Validation loss " + str(history.history['val_loss'])+ "\n\n")
f.write("Elapsed time: " + str(elapsed_time))
f.write('\n\nTest loss: ' + str(evaluations[0]) + ', test acc: ' + str(evaluations[1]))
f.write('\n' + str(sum(y_pred==classes)/test_set_size))
f.write('\n')
f.write(str(confusion_matrix(test_batches.classes[test_batches.index_array],y_pred)))
f.close()
