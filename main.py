import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler
import torch
import numpy as np
import torch.optim as optim



class LeNet(nn.Module):
  
  def __init__(self):
    super().__init__()
    
    
    self.conv1 = nn.Conv2d(3, 20, 5, 1)
    self.conv2 = nn.Conv2d(20, 50, 5, 1)
    self.fc1 = nn.Linear(5*5*50, 500)
    self.dropout1 = nn.Dropout(0.5)
    self.fc2 = nn.Linear(500, 10)

    '''self.conv1 = nn.Conv2d(3, 64, 5,1)
    print(self.conv1)
    self.conv2 = nn.Conv2d(64, 64, 5,1)
    print(self.conv2)
    self.conv3 = nn.Conv2d(64, 96, 4,1)
    print(self.conv3)
    self.conv4 = nn.Conv2d(96, 96, 4,1)
    print(self.conv4)
    self.conv5 = nn.Conv2d(96, 128, 3,1)
    print(self.conv5)
    self.conv6 = nn.Conv2d(128, 128, 3,1)
    print(self.conv6)
    self.fc1   = nn.Linear(128*4*4, 1024)
    print(self.fc1)
    self.fc2   = nn.Linear(1024, 512)
    self.fc3   = nn.Linear(512, 10)
    self.dropout1 = nn.Dropout(0.10)'''


  def forward(self, x):
    
    x = F.relu(self.conv1(x))
    x = F.max_pool2d(x, 2, 2)
    x = F.relu(self.conv2(x))
    x = F.max_pool2d(x, 2, 2)
    # flatten
    x = x.view(-1, 5*5*50)
    x = F.relu(self.fc1(x))
    x = self.dropout1(x)
    x = self.fc2(x)
    return x
    '''out = F.relu(self.conv1(x))
    out= self.dropout1(out)
    out = F.relu(self.conv2(out))
    out = F.max_pool2d(out, 2,2)
    print(out)

    out = F.relu(self.conv3(out))
    out= self.dropout1(out)
    out = F.relu(self.conv4(out))
    out = F.max_pool2d(out, 2,2)  
    print(out)     

    out = F.relu(self.conv5(out))
    out= self.dropout1(out)
    out = F.relu(self.conv6(out))
    out = F.max_pool2d(out, 2,2)
    print(out)

    out = out.view(out.size(0), -1)
    out = F.relu(self.fc1(out))
    out = F.relu(self.fc2(out))
    out = self.fc3(out)
    return out'''

    


transformer = transforms.Compose([transforms.Resize((32, 32)),
                                  transforms.ToTensor(),
                                  transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                                 ])

training_dataset = datasets.CIFAR10(root='./data', train=True, download=True, transform=transformer)
validation_dataset = datasets.CIFAR10(root='./data', train=False, download=True, transform=transformer)

training_loader = torch.utils.data.DataLoader(dataset=training_dataset, batch_size=100, shuffle=True)
validation_loader = torch.utils.data.DataLoader(dataset=validation_dataset, batch_size=100, shuffle=False)

def im_convert(tensor):
  image = tensor.clone().detach().numpy()
  image = image.transpose(1, 2, 0)
  image = image * np.array([0.5, 0.5, 0.5] + np.array([0.5, 0.5, 0.5]))
  image = image.clip(0, 1)
  return image

classes = ("plane", "car", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck")
train_on_gpu = torch.cuda.is_available()

if not train_on_gpu:
    print('CUDA is not available.  Training on CPU ...')
else:
    print('CUDA is available!  Training on GPU ...')

dataiter = iter(training_loader)
images, labels = dataiter.next()

fig = plt.figure(figsize=(25, 4))

for i in np.arange(20):
  # row 2 column 10
  ax = fig.add_subplot(2, 10, i+1, xticks=[], yticks=[])
  plt.imshow(im_convert(images[i]))
  ax.set_title(classes[labels[i].item()])

model =LeNet()
print(model)
#sys.exit(-1)
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)


epochs = 20
running_loss_history = []
running_correct_history = []
validation_running_loss_history = [] 
validation_running_correct_history = []
device = 'cuda' if torch.cuda.is_available() else 'cpu'

for e in range(epochs):
  
  running_loss = 0.0
  running_correct = 0.0
  validation_running_loss = 0.0
  validation_running_correct = 0.0
  
  for inputs, labels in training_loader:   
    if train_on_gpu:
        inputs, labels = inputs.cuda(), labels.cuda() 
     
    #inputs = inputs.to(device)
    #labels = labels.to(device)
    optimizer.zero_grad()
    outputs = model(inputs)
    loss = criterion(outputs, labels)
    loss.backward()
    optimizer.step()
    _, preds = torch.max(outputs, 1)
    running_correct += torch.sum(preds == labels.data)
    running_loss += loss.item()

    
    
    
  else:    
    
    with torch.no_grad():
      
      for val_input, val_label in validation_loader:
        
        val_input = val_input.to(device)
        val_label = val_label.to(device)
        val_outputs = model(val_input)
        val_loss = criterion(val_outputs, val_label)
        
        _, val_preds = torch.max(val_outputs, 1)
        validation_running_loss += val_loss.item()
        validation_running_correct += torch.sum(val_preds == val_label.data) 
    
    
    epoch_loss = running_loss / len(training_loader)
    epoch_acc = running_correct.float() / len(training_loader)
    running_loss_history.append(epoch_loss)
    running_correct_history.append(epoch_acc)
    
    val_epoch_loss = validation_running_loss / len(validation_loader)
    val_epoch_acc = validation_running_correct.float() / len(validation_loader)
    validation_running_loss_history.append(val_epoch_loss)
    validation_running_correct_history.append(val_epoch_acc)
    
    print("===================================================")
    print("epoch: ", e + 1)
    print("training loss: {:.5f}, acc: {:5f}".format(epoch_loss, epoch_acc))
    print("validation loss: {:.5f}, acc: {:5f}".format(val_epoch_loss, val_epoch_acc))








